package com.returnTest.comReturnTest.controller

import com.returnTest.comReturnTest.dataClass.*
import com.returnTest.comReturnTest.entity.Orders
import com.returnTest.comReturnTest.service.OrderService
import com.returnTest.comReturnTest.service.ReturnService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.util.*

class MainControllerTest {

    @Mock
    private lateinit var orderService: OrderService

    @Mock
    private lateinit var returnService: ReturnService

    @InjectMocks
    private lateinit var mainController: MainController

    @BeforeEach
    fun setup() {
        MockitoAnnotations.openMocks(this)
    }

    companion object {
        private val order = Orders(
            id = 1,
            orderId = "Test",
            emailAddress = "Test@gmail.com",
            itemName = "Test Item",
            sku = "string",
            quantity = 5L,
            price = 10.00
        )
    }

    @Test
    fun `success get all data`() {

        val orderData = listOf<Orders>(order, order)

        Mockito.`when`(orderService.getAllData()).thenReturn(orderData)

        val response = mainController.getAllOrder()

        Assertions.assertEquals(
            response, ResponseEntity(
                Response(
                    message = "success",
                    data = orderData,
                    status = "OK"
                ),
                HttpStatus.OK
            )
        )
    }

    @Test
    fun `success create pending return`() {
        val token = UUID.randomUUID().toString()
        val request = PendingReturnRequest(
            orderNumber = "xxxx",
            emailAddress = "xxxx"
        )
        Mockito.`when`(returnService.pendingReturn(request)).thenReturn(token)
        val response = mainController.doPendingReturn(
            request
        )

        Assertions.assertEquals(
            response, ResponseEntity(
                Response(
                    message = "success",
                    data = token,
                    status = "OK"
                ),
                HttpStatus.OK
            )
        )
    }

    @Test
    fun `success create return`() {
        val token = UUID.randomUUID().toString()
        val request = ReturnRequest(
            token = token,
            orders = listOf()
        )
        val returnResponse = ReturnResponse(
            id = 2,
            returnStatus = "xxx",
            totalAmount = 00.0
        )
        Mockito.`when`(returnService.doCreateReturn(request)).thenReturn(returnResponse)
        val response = mainController.doReturns(
            request
        )

        Assertions.assertEquals(
            response, ResponseEntity(
                Response(
                    message = "success",
                    data = returnResponse,
                    status = "OK"
                ),
                HttpStatus.OK
            )
        )
    }

    @Test
    fun `success get return detail`() {
        val returnItemDetailResponse = ReturnItemDetailResponse(
            id = 1,
            orderNumber = "xxx",
            status = "xxx",
            quantity = 1L,
            amount = 0.0,
            sku = "xxx",
            itemName = "xxx",
            price = 0.0
        )
        val resultResponse = ReturnDetailResponse(
            returnId = 1,
            status = "AWAITING_APPROVAL",
            totalAmount = 0.0,
            orders = listOf(returnItemDetailResponse)
        )
        Mockito.`when`(returnService.doGetReturnById(1)).thenReturn(resultResponse)
        val response = mainController.doGetDetailReturn(1)
        Assertions.assertEquals(
            response, ResponseEntity(
                Response(
                    message = "success",
                    data = resultResponse,
                    status = "OK"
                ),
                HttpStatus.OK
            )
        )
    }

    @Test
    fun `success update return item`() {
        val request = UpdateItemRequest(
            status = "xxx"
        )
        Mockito.`when`(returnService.doUpdateReturnItem(1, 2, request)).thenReturn("OK")
        val response = mainController.doUpdateReturnItem(1, 2, request)
        Assertions.assertEquals(
            response, ResponseEntity(
                Response(
                    message = "success",
                    data = "OK",
                    status = "OK"
                ),
                HttpStatus.OK
            )
        )
    }
}