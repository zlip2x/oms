drop table IF EXISTS orders;

create TABLE orders (
  order_id INT AUTO_INCREMENT,
  order_number VARCHAR(250) NOT NULL,
  email_address VARCHAR(250) NOT NULL,
  item_name VARCHAR(250) NOT NULL,
  sku VARCHAR(250) NOT NULL,
  quantity INT NOT NULL,
  price DECIMAL(14,2) NOT NULL,
  PRIMARY KEY (order_id)
);

drop table IF EXISTS return_token;
create TABLE return_token (
    return_token_id INT AUTO_INCREMENT,
    return_token VARCHAR(250) NOT NULL,
    return_token_order_number VARCHAR(50) NOT NULL,
    return_token_status VARCHAR(50) NOT NULL,
    return_token_created_at DATE NOT NULL,
    PRIMARY KEY (return_token_id)
);

drop table IF EXISTS returns;
create TABLE returns (
    return_id INT AUTO_INCREMENT,
    return_amount DECIMAL(14,2) NOT NULL,
    return_status VARCHAR(50) NOT NULL,
    return_created_at DATE NOT NULL,
    return_updated_at DATE NULL,
    PRIMARY KEY (return_id)
);

drop table IF EXISTS return_items;
create TABLE return_items(
    return_item_id INT AUTO_INCREMENT,
    return_id INT NOT NULL,
    return_item_order_id INT NOT NULL,
    return_item_sku VARCHAR(50) NULL,
    return_item_quantity INT NULL,
    return_item_status VARCHAR(50) NOT NULL,
    return_item_amount DECIMAL(14,2) NOT NULL,
    return_item_created_at DATE NOT NULL,
    return_item_updated_at DATE NULL,
    PRIMARY KEY (return_item_id)
)