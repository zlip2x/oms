package com.returnTest.comReturnTest.entity

import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import javax.persistence.*

@Entity
@Table(name = "orders")
@DynamicInsert
@DynamicUpdate
data class Orders(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "order_id", nullable = false)
    var id: Long? = null,

    @Column(name = "order_number", nullable = false)
    var orderId: String? = "",

    @Column(name = "email_address", nullable = false)
    var emailAddress: String? = "",

    @Column(name = "item_name", nullable = false)
    var itemName: String? = "",

    @Column(name = "sku", nullable = false)
    var sku: String? = "",

    @Column(name = "quantity", nullable = false)
    var quantity: Long? = 0,

    @Column(name = "price", nullable = false)
    var price: Double? = 0.0

)

