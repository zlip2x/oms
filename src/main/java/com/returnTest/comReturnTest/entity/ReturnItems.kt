package com.returnTest.comReturnTest.entity

import com.fasterxml.jackson.annotation.JsonFormat
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import java.time.LocalDateTime
import java.util.Date
import javax.persistence.*

@Entity
@Table(name = "return_items")
@DynamicInsert
@DynamicUpdate
data class ReturnItems(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "return_item_id", nullable = false)
    var id: Long? = null,

    @Column(name = "return_id", nullable = false)
    var returnId: Int? = null,

    @Column(name = "return_item_order_id", nullable = false)
    var orderId: Int? = null,

    @Column(name = "return_item_sku", nullable = false)
    var sku: String? = null,

    @Column(name = "return_item_quantity", nullable = false)
    var quantity: Long? = 0,

    @Column(name = "return_item_amount", nullable = false)
    var amount: Double? = 0.0,

    @Column(name = "return_item_status", nullable = false)
    var status: String? = null,

    @Column(name = "return_item_created_at", nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
    var createdAt: Date? = null,

    @Column(name = "return_item_updated_at", nullable = true)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
    var updatedAt: Date? = null,
)
