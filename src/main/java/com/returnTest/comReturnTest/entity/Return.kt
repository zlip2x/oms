package com.returnTest.comReturnTest.entity

import com.fasterxml.jackson.annotation.JsonFormat
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import java.time.LocalDateTime
import java.util.Date
import javax.persistence.*

@Entity
@Table(name = "returns")
@DynamicInsert
@DynamicUpdate
data class Return(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "return_id", nullable = false)
    var id: Long? = null,

    @Column(name = "return_amount", nullable = false)
    var amount: Double? = null,

    @Column(name = "return_status", nullable = false)
    var returnStatus: String? = null,

    @Column(name = "return_created_at", nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
    var createdAt: Date? = null,

    @Column(name = "return_updated_at", nullable = true)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
    var updatedAt: Date? = null,
)
