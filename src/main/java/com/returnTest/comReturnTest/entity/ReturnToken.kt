package com.returnTest.comReturnTest.entity

import com.fasterxml.jackson.annotation.JsonFormat
import org.hibernate.annotations.DynamicInsert
import org.hibernate.annotations.DynamicUpdate
import java.time.LocalDateTime
import java.util.Date
import javax.persistence.*

@Entity
@Table(name = "return_token")
@DynamicInsert
@DynamicUpdate
data class ReturnToken(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "return_token_id", nullable = false)
    var id: Long? = null,

    @Column(name = "return_token", nullable = false)
    var returnToken : String? = null,

    @Column(name = "return_token_order_number", nullable = false)
    var orderId: String? = null,

    @Column(name = "return_token_status", nullable = false)
    var status: String? = null,

    @Column(name = "return_token_created_at", nullable = false)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "Asia/Jakarta")
    var createdAt: Date? = null,
)
