package com.returnTest.comReturnTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComReturnTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComReturnTestApplication.class, args);
	}

}
