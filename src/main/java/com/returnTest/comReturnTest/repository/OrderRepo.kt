package com.returnTest.comReturnTest.repository

import com.returnTest.comReturnTest.entity.Orders
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface OrderRepo: JpaRepository<Orders, Long> {
    fun findFirstByOrderIdAndEmailAddress(orderNumber: String, emailAddress: String): Orders?

    fun findFirstByOrderId(ordersNumber: String): Orders?

    fun findByOrderId(orderNumber: String): List<Orders>

    fun findByOrderIdAndSku(orderNumber: String, sku: String): List<Orders>
}