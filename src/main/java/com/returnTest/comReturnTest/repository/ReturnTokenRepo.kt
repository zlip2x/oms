package com.returnTest.comReturnTest.repository

import com.returnTest.comReturnTest.entity.ReturnToken
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ReturnTokenRepo: JpaRepository<ReturnToken, Long> {
    fun findFirstByReturnTokenAndStatus(token: String, status: String): ReturnToken?
}