package com.returnTest.comReturnTest.repository

import com.returnTest.comReturnTest.entity.ReturnItems
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ReturnItemRepo: JpaRepository<ReturnItems, Long> {
    fun findFirstBySkuAndStatus(sku: String, status: String): ReturnItems?

    fun findByReturnId(returnId: Int): List<ReturnItems>
}