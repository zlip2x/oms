package com.returnTest.comReturnTest.service

import com.returnTest.comReturnTest.dataClass.*

interface ReturnService {
    fun pendingReturn(request: PendingReturnRequest): String

    fun doCreateReturn(request: ReturnRequest): ReturnResponse

    fun doGetReturnById(id: Int): ReturnDetailResponse

    fun doUpdateReturnItem(returnId: Int, itemId: Int, request: UpdateItemRequest): String
}