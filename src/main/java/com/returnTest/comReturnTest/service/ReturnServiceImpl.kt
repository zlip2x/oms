package com.returnTest.comReturnTest.service

import com.returnTest.comReturnTest.dataClass.*
import com.returnTest.comReturnTest.entity.Orders
import com.returnTest.comReturnTest.entity.Return
import com.returnTest.comReturnTest.entity.ReturnItems
import com.returnTest.comReturnTest.entity.ReturnToken
import com.returnTest.comReturnTest.exception.AppException
import com.returnTest.comReturnTest.repository.OrderRepo
import com.returnTest.comReturnTest.repository.ReturnItemRepo
import com.returnTest.comReturnTest.repository.ReturnRepo
import com.returnTest.comReturnTest.repository.ReturnTokenRepo
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import java.util.*

@Service
class ReturnServiceImpl(
    private val orderRepo: OrderRepo,
    private val returnTokenRepo: ReturnTokenRepo,
    private val returnItemRepo: ReturnItemRepo,
    private val returnRepo: ReturnRepo
) : ReturnService {

    companion object {
        private const val AWAITING_APPROVAL = "AWAITING_APPROVAL"
        private const val COMPLETE = "COMPLETE"
        private const val REJECTED = "REJECTED"
        private const val ACCEPTED = "ACCEPTED"
        private val VALID_STATUS = listOf<String>("REJECTED", "ACCEPTED")
    }

    override fun pendingReturn(request: PendingReturnRequest): String {
        if (request.orderNumber.isEmpty()) throw AppException("Order Number is required")
        if (request.emailAddress.isEmpty()) throw AppException("Email Address is required")
        val orderData = orderRepo.findFirstByOrderIdAndEmailAddress(request.orderNumber, request.emailAddress)
            ?: throw AppException("Order Not Found")
        val returnToken = UUID.randomUUID().toString()
        returnTokenRepo.saveAndFlush(
            ReturnToken(
                returnToken = returnToken,
                orderId = orderData.orderId,
                status = "CREATED",
                createdAt = Date()
            )
        )
        return returnToken
    }

    override fun doCreateReturn(request: ReturnRequest): ReturnResponse {

        if (request.token.isEmpty()) throw AppException("Return token is required")

        val returnTokenData = returnTokenRepo.findFirstByReturnTokenAndStatus(request.token, "CREATED")
            ?: throw AppException("Return token not found")

        val orderData = orderRepo.findFirstByOrderId(returnTokenData.orderId ?: "")
            ?: throw AppException("Order Number not valid")

        val returnItemData =
            orderData.sku?.let { returnItemRepo.findFirstBySkuAndStatus(it, AWAITING_APPROVAL) }

        if (returnItemData != null) throw AppException("Item Return is in progress")

        var totalAmount = 0.0
        var returnData = returnRepo.saveAndFlush(
            Return(
                amount = totalAmount,
                returnStatus = AWAITING_APPROVAL,
                createdAt = Date()
            )
        )

        var ordersData = mutableListOf<Orders>()
        if (request.orders?.isEmpty() == false) {
            request.orders.forEach {
                orderRepo.findByOrderIdAndSku(it.orderNumber, it.sku).forEach { order ->
                    ordersData.add(
                        Orders(
                            id = order.id,
                            orderId = order.orderId,
                            sku = order.sku,
                            quantity = it.quantity.toLong(),
                            emailAddress = order.emailAddress,
                            price = order.price,
                            itemName = order.itemName
                        )
                    )
                }
            }
        } else {
            orderRepo.findByOrderId(returnTokenData.orderId ?: "").forEach {
                ordersData.add(it)
            }
        }

        ordersData.forEach {
            val amount = (it.quantity?.let { it1 -> it.price?.times(it1) }) ?: 0.0
            totalAmount += amount
            returnItemRepo.saveAndFlush(
                ReturnItems(
                    returnId = returnData.id?.toInt(),
                    orderId = it.id?.toInt(),
                    sku = it.sku,
                    quantity = it.quantity,
                    status = AWAITING_APPROVAL,
                    amount = amount,
                    createdAt = Date()
                )
            )
        }

        returnData = returnRepo.saveAndFlush(
            returnData.apply {
                this.amount = totalAmount
            }
        )

        returnTokenRepo.saveAndFlush(
            returnTokenData.apply {
                status = "USED"
            }
        )

        return ReturnResponse(
            id = returnData.id?.toInt(),
            returnStatus = returnData.returnStatus,
            totalAmount = returnData.amount
        )
    }

    override fun doGetReturnById(id: Int): ReturnDetailResponse {
        val returnData = returnRepo.findByIdOrNull(id.toLong()) ?: throw AppException("Return not found")

        val returnItemData = mutableListOf<ReturnItemDetailResponse>()

        returnItemRepo.findByReturnId(id).forEach {
            val orderData = orderRepo.findByIdOrNull(it.orderId?.toLong())
            if (orderData != null) {
                returnItemData.add(
                    ReturnItemDetailResponse(
                        id = it.id?.toInt() ?: 0,
                        orderNumber = orderData.orderId ?: "",
                        status = it.status ?: "",
                        price = orderData.price ?: 0.0,
                        amount = it.quantity?.let { it1 -> orderData.price?.times(it1) } ?: 0.0,
                        sku = orderData.sku ?: "",
                        itemName = orderData.itemName ?: "",
                        quantity = it.quantity ?: 0L
                    )
                )
            }
        }

        return ReturnDetailResponse(
            returnId = returnData.id?.toInt() ?: 0,
            status = returnData.returnStatus ?: "",
            totalAmount = returnData.amount ?: 0.0,
            orders = returnItemData
        )
    }

    override fun doUpdateReturnItem(returnId: Int, itemId: Int, request: UpdateItemRequest): String {
        val returnData = returnRepo.findByIdOrNull(returnId.toLong()) ?: throw AppException("Return not found")
        val returnItemData =
            returnItemRepo.findByIdOrNull(itemId.toLong()) ?: throw AppException("Return Item not found")
        if (!VALID_STATUS.contains(request.status)) throw AppException("Status not valid")

        returnItemRepo.saveAndFlush(
            returnItemData.apply {
                this.status = request.status
                this.updatedAt = Date()
            }
        )

        val returnItems = returnItemRepo.findByReturnId(returnId)
        var amount = returnItems.sumOf { it.amount ?: 0.0 }
        val count = returnItems.count()
        var waitingApprovalItem = 0
        var rejectedItem = 0

        returnItems.forEach {
            if (it.status == REJECTED) {
                amount = amount.minus(it.amount!!)
                rejectedItem = rejectedItem.plus(1)
            }

            if (it.status == AWAITING_APPROVAL) {
                waitingApprovalItem = waitingApprovalItem.plus(1)
            }
        }

        returnRepo.saveAndFlush(
            returnData.apply {
                this.returnStatus = when {
                    rejectedItem == count -> REJECTED
                    waitingApprovalItem == 0 -> COMPLETE
                    else -> returnData.returnStatus
                }
                this.amount = amount
                this.updatedAt = Date()
            }
        )

        return "OK"
    }
}