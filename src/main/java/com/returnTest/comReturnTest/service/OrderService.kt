package com.returnTest.comReturnTest.service

import com.returnTest.comReturnTest.dataClass.PendingReturnRequest
import com.returnTest.comReturnTest.entity.Orders

interface OrderService {
    fun getAllData(): List<Orders>
}