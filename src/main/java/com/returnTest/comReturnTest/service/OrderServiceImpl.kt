package com.returnTest.comReturnTest.service

import com.returnTest.comReturnTest.entity.Orders
import com.returnTest.comReturnTest.repository.OrderRepo
import org.springframework.stereotype.Service

@Service
class OrderServiceImpl(
    private val orderRepo: OrderRepo
) : OrderService {
    override fun getAllData(): List<Orders> {
        return orderRepo.findAll()
    }
}