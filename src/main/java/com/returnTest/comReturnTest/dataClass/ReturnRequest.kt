package com.returnTest.comReturnTest.dataClass

import com.fasterxml.jackson.annotation.JsonProperty

data class ReturnRequest(
    @JsonProperty("token")
    val token: String,

    @JsonProperty("orders")
    val orders: List<OrderReturnRequest>?
)

data class OrderReturnRequest(
    @JsonProperty("orderNumber")
    val orderNumber: String,

    @JsonProperty("sku")
    val sku: String,

    @JsonProperty("quantity")
    val quantity: Int
)