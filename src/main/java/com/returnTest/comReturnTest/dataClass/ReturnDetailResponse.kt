package com.returnTest.comReturnTest.dataClass

import com.fasterxml.jackson.annotation.JsonProperty

data class ReturnDetailResponse(
    @JsonProperty("id")
    val returnId: Int,

    @JsonProperty("status")
    val status: String,

    @JsonProperty("totalAmount")
    val totalAmount: Double,

    @JsonProperty("orders")
    val orders: List<ReturnItemDetailResponse>,
)

data class ReturnItemDetailResponse(
    @JsonProperty("id")
    val id: Int,

    @JsonProperty("orderNumber")
    val orderNumber: String,

    @JsonProperty("status")
    val status: String,

    @JsonProperty("quantity")
    val quantity: Long,

    @JsonProperty("price")
    val price: Double,

    @JsonProperty("amount")
    val amount: Double,

    @JsonProperty("sku")
    val sku: String,

    @JsonProperty("itemName")
    val itemName: String
)