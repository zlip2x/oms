package com.returnTest.comReturnTest.dataClass

import com.fasterxml.jackson.annotation.JsonProperty

data class PendingReturnRequest(
    @JsonProperty("orderNumber")
    val orderNumber: String,

    @JsonProperty("emailAddress")
    val emailAddress: String
)
