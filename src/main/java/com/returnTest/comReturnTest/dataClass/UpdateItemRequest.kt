package com.returnTest.comReturnTest.dataClass

import com.fasterxml.jackson.annotation.JsonProperty

class UpdateItemRequest(
    @JsonProperty("status")
    val status: String?
)