package com.returnTest.comReturnTest.dataClass

data class Response<T>(
    var message: String,
    var data: T? = null,
    var status: String
)