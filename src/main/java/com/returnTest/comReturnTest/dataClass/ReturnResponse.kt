package com.returnTest.comReturnTest.dataClass

import com.fasterxml.jackson.annotation.JsonProperty

data class ReturnResponse(
    @JsonProperty("id")
    val id: Int?,

    @JsonProperty("returnStatus")
    val returnStatus: String?,

    @JsonProperty("totalAmount")
    val totalAmount: Double?
)
