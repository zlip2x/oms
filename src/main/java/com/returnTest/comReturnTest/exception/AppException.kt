package com.returnTest.comReturnTest.exception

import java.lang.RuntimeException

class AppException(vararg defaultMessage: String): RuntimeException(defaultMessage.joinToString("::"))