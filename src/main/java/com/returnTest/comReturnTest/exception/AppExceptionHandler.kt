package com.returnTest.comReturnTest.exception

import com.returnTest.comReturnTest.dataClass.Response
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class AppExceptionHandler @Autowired constructor() {

    @ExceptionHandler(Exception::class)
    fun exception(ex: Exception) = throwException(ex, HttpStatus.BAD_REQUEST)

    @ExceptionHandler(AppException::class)
    fun appException(ex: AppException) = throwException(ex, HttpStatus.BAD_REQUEST)

    private fun throwException(
        throwable: Throwable,
        httpStatus: HttpStatus,
        message: String = ""
    ): ResponseEntity<Response<Any>> {
        var bodyMessage = throwable.localizedMessage
        if(message != "") bodyMessage = message
        return ResponseEntity(
            Response(
                message = bodyMessage,
                data = null,
                status = "NOK"
            ),
            HttpStatus.BAD_REQUEST
        )
    }

}