package com.returnTest.comReturnTest.controller

import com.returnTest.comReturnTest.dataClass.PendingReturnRequest
import com.returnTest.comReturnTest.dataClass.Response
import com.returnTest.comReturnTest.dataClass.ReturnRequest
import com.returnTest.comReturnTest.dataClass.UpdateItemRequest
import com.returnTest.comReturnTest.service.OrderService
import com.returnTest.comReturnTest.service.ReturnService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class MainController(
    private val orderService: OrderService,
    private val returnService: ReturnService
) : BaseController() {

    @GetMapping("/")
    fun getAllOrder(): ResponseEntity<Response<Any>> = response(orderService.getAllData())


    @PostMapping("/pending/returns")
    fun doPendingReturn(
        @RequestBody request: PendingReturnRequest
    ): ResponseEntity<Response<Any>> = response(returnService.pendingReturn(request))

    @PostMapping("/returns")
    fun doReturns(
        @RequestBody request: ReturnRequest
    ): ResponseEntity<Response<Any>> = response(returnService.doCreateReturn(request))


    @GetMapping("/returns/{id}")
    fun doGetDetailReturn(
        @PathVariable("id") returnId: Int
    ): ResponseEntity<Response<Any>> = response(returnService.doGetReturnById(returnId))

    @PutMapping("returns/{id}/items/{itemId}/qc/status")
    fun doUpdateReturnItem(
        @PathVariable("id") returnId: Int,
        @PathVariable("itemId") itemId: Int,
        @RequestBody request: UpdateItemRequest
    ): ResponseEntity<Response<Any>> = response(returnService.doUpdateReturnItem(returnId, itemId, request))

}
