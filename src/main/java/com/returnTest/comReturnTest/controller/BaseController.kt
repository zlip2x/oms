package com.returnTest.comReturnTest.controller

import com.returnTest.comReturnTest.dataClass.Response
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

open class BaseController {
    companion object {
        private const val OK = "OK"
        private const val NOK = "NOK"
        private const val SUCCESS = "success"
        private const val FAILED = "failed"
    }

    protected fun response(any: Any): ResponseEntity<Response<Any>> {
        return ResponseEntity(
            Response(
                message = SUCCESS,
                data = any,
                status = OK
            ),
            HttpStatus.OK
        )
    }
}